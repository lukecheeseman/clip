module Main where

import Editor
import Options.Applicative
import Data.Semigroup ((<>))

data EditorOptions = EditorOptions {
                                     file :: String
                                   }

editorOptionParser :: Parser EditorOptions
editorOptionParser = EditorOptions
      <$> argument str (metavar "FILE")

main :: IO ()
main = do
  options <- execParser opts
  editorRun (file options)
  where
    opts :: ParserInfo EditorOptions
    opts = info (editorOptionParser <**> helper)
      ( fullDesc
     <> progDesc "A basic text editor")
