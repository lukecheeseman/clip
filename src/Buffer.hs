module Buffer (
  EBuf, ERow,

  Buffer.empty, emptyRow, Buffer.null,

  bufLength, rowLength,

  rowAt,

  insertRowAt, replaceRowAt, deleteRowAt,
  insertCharAt, replaceCharAt, deleteCharAt,
  takeRows, dropRows, sliceRows, mapRows,

  foldlRow,

  takeChars, dropChars,

  pack, unpackToText, unpackToString
) where

import Control.Lens
import Control.Monad.IO.Class

import qualified Data.Text as T

import Data.Vector as V

type ERow = T.Text
type EBuf = Vector ERow

empty :: EBuf
empty = V.empty

emptyRow :: ERow
emptyRow = T.empty

null :: EBuf -> Bool
null = (==) Buffer.empty

-- nullRow :: Int -> EBuf -> Bool
-- nullRow = (==) Buffer.empty

bufLength :: EBuf -> Int
bufLength = V.length

rowLength :: Int -> EBuf -> Int
rowLength x b = case b !? x of
  Just r -> T.length r
  Nothing -> 0

insertRowAt :: Int -> T.Text -> EBuf -> EBuf
insertRowAt x t = uncurry (V.++) . (_2 %~ V.cons t) . V.splitAt x

replaceRowAt :: Int -> T.Text -> EBuf -> EBuf
replaceRowAt x t = ix x .~ t

deleteRowAt :: Int -> EBuf -> EBuf
deleteRowAt x = uncurry (V.++) . (_2 %~ V.tail) . V.splitAt x

insertCharAt :: Int -> Int -> Char -> EBuf -> EBuf
insertCharAt x y c = ix x %~ (uncurry T.append . (_2 %~ T.cons c) . T.splitAt y)

replaceCharAt :: Int -> Int -> Char -> EBuf -> EBuf
replaceCharAt x y c = (ix x . ix y) .~ c

deleteCharAt :: Int -> Int -> EBuf -> EBuf
deleteCharAt x y = ix x %~ (uncurry T.append . (_2 %~ T.tail) . T.splitAt y)

pack :: String -> EBuf
pack = fromList . T.lines . T.pack

unpackToText :: EBuf -> T.Text
unpackToText buf =
  let newlines = V.map (flip T.append (T.singleton '\n')) buf in
    V.foldl T.append T.empty newlines

unpackToString :: EBuf -> String 
unpackToString = T.unpack . unpackToText

rowAt :: EBuf -> Int -> Maybe ERow
rowAt = (!?)

takeRows :: Int -> EBuf -> EBuf
takeRows = V.take

dropRows :: Int -> EBuf -> EBuf
dropRows = V.drop

sliceRows :: Int -> Int -> EBuf -> EBuf
sliceRows = V.slice

mapRows :: (ERow -> ERow) -> EBuf -> EBuf
mapRows = V.map

-- Row utlitiles

mapRow :: (Char -> Char) -> ERow -> ERow
mapRow = T.map

foldlRow :: (a -> Char -> a) -> a -> ERow -> a
foldlRow = T.foldl

takeChars :: Int -> ERow -> ERow
takeChars = T.take

dropChars :: Int -> ERow -> ERow
dropChars = T.drop
