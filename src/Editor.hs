{-# LANGUAGE TemplateHaskell #-}

module Editor
    (editorRun) where

import Control.Lens

import Control.Monad
import Control.Monad.Loops
import Control.Monad.State

import Data.Bits
import Data.Char
import qualified Data.Text as T

import System.Directory
import System.Exit
import System.IO

import UI.NCurses

import Buffer

data EFlags = EFlags {
              _exit :: Bool,
              _dirty :: Bool
            }
makeLenses ''EFlags

data ECursor = ECursor { _row :: Int, _col :: Int}
makeLenses ''ECursor

data EConf = EConf {
              _tabStop :: Int
            }
makeLenses ''EConf

data EFields = EFields {
              _flags ::EFlags,
              _window :: Window,
              _filename :: String,
              _buf :: EBuf,
              _rowoff :: Int,
              _coloff :: Int,
              _cursor :: ECursor,
              _conf :: EConf
              }
makeLenses ''EFields

type EState = StateT EFields Curses

editorSetDirty :: EState ()
editorSetDirty = flags.dirty .= True

-- Would be good to have some event handler with different modes
-- So perhaps a type class
editorProcessEvent :: Event -> EState ()
editorProcessEvent (EventCharacter e)
  | isControl e = processControlCharacter e
  | otherwise = do
    s <- get
    buf %= insertCharAt (s^.cursor.row) (s^.cursor.col) e
    editorMoveCursorCol 1
    editorSetDirty
  where
    processControlCharacter :: Char -> EState ()
    processControlCharacter e
      -- FIXME: duh'doi newline should break the current line
      | e == '\n' = do
        s <- get
        buf %= insertRowAt (s^.cursor.row + 1) emptyRow
        editorSetDirty
        editorMoveCursorRow 1
      | e == controlChar 'd' = do
        s <- get
        buf %= deleteRowAt (s^.cursor.row)
        editorSetDirty
      | e == controlChar 'q' = (flags . exit) .= True
      | e == controlChar 's' = editorSave
      | otherwise = return ()
    controlChar :: Char -> Char
    controlChar = (chr . (.&.0x1f) . ord)
editorProcessEvent (EventSpecialKey e) = do
  s <- get
  case e of
    KeyUpArrow -> editorMoveCursorRow (-1)
    KeyDownArrow -> editorMoveCursorRow 1
    KeyLeftArrow -> editorMoveCursorCol (-1)
    KeyRightArrow -> editorMoveCursorCol 1
    KeyBackspace -> do
      buf %= deleteCharAt (s^.cursor.row) ((s^.cursor.col) - 1)
      editorSetDirty
      editorMoveCursorCol (-1)
    KeyDeleteCharacter -> do
      buf %= deleteCharAt (s^.cursor.row) (s^.cursor.col)
      editorSetDirty
    KeyHome -> editorMoveCursorCol (-s^.cursor.col)
    KeyEnd -> editorMoveCursorCol (rowLength (s^.cursor.row) (s^.buf) - s^.cursor.col)
    KeyPreviousPage -> do
      (rows, cols) <- lift $ updateWindow (s^.window) windowSize
      editorMoveCursorRow (s^.rowoff - s^.cursor.row - fromIntegral rows)
    KeyNextPage -> do
      (rows, cols) <- lift $ updateWindow (s^.window) windowSize
      editorMoveCursorRow (fromIntegral (2 * rows) - (s^.cursor.row - s^.rowoff) - 1)
    otherwise -> return ()
editorProcessEvent (EventMouse i ms) = do
  s <- get
  let (x, y, z) = mouseCoordinates ms
  (crow, ccol) <- lift $ getCursor (s^.window)
  editorMoveCursor (fromIntegral (x - ccol)) (fromIntegral (y - crow))
editorProcessEvent _ = editorSetDirty

editorMoveCursorRow :: Int -> EState ()
editorMoveCursorRow = editorMoveCursor 0

editorMoveCursorCol :: Int -> EState ()
editorMoveCursorCol = flip editorMoveCursor 0

editorMoveCursor :: Int -> Int -> EState ()
editorMoveCursor dx dy = do
  s <- get
  let (newrow, newcol) = (s^.cursor.row + fromIntegral dy, s^.cursor.col + fromIntegral dx)
  cursor.row .= max 0 (min newrow (bufLength (s^.buf) - 1))
  cursor.col .= max 0 (min newcol (rowLength newrow (s^.buf)))
  editorSetDirty

editorCursorColToRenderCol :: EState (Int)
editorCursorColToRenderCol = do
  s <- get
  case rowAt (s^.buf) (s^.cursor.row)  of
    Just r -> (return . foldlRow (increaseRenderCol (s^.conf)) 0 . takeChars (s^.cursor.col)) r
    Nothing -> return 0
  where
  increaseRenderCol :: EConf -> Int -> Char -> Int
  increaseRenderCol conf rx c = do
    if c == '\t' then
      rx + (conf^.tabStop ) - (rx `mod` conf^.tabStop)
    else
      rx + 1

editorScroll :: EState ()
editorScroll = do
  s <- get
  (rows, cols) <- lift $ updateWindow (s^.window) windowSize
  if s^.cursor.row < s^.rowoff then
    rowoff .= s^.cursor.row
  else if s^.cursor.row >= fromIntegral rows + s^.rowoff - 1 then
    rowoff .= s^.cursor.row - fromIntegral rows + 1
  else
    return ()

  renderCol <- editorCursorColToRenderCol
  if renderCol < s^.coloff then
    coloff .= renderCol
  else if renderCol >= fromIntegral cols + s^.coloff - 1 then
    coloff .= renderCol - fromIntegral cols + 1
  else
    return ()

editorDrawRows :: EState ()
editorDrawRows = do
    s <- get
    flags.dirty .= False
    if Buffer.null (s^.buf) then do
      lift $ updateWindow (s^.window) $ do
        (rows, cols) <- windowSize
        moveCursor (rows `div` 2) 0
        let welcome = T.pack "Welcome to clip"
        let padding = T.replicate ((fromIntegral cols - T.length welcome) `div` 2) (T.singleton ' ')
        drawText $ T.append (T.append padding welcome) padding
        moveCursor 0 0
    else if (s^.flags.dirty) then do
      lift $ updateWindow (s^.window) $ do
        (crow, ccol) <- cursorPosition
        moveCursor 0 0
        (rows, cols) <- windowSize
        let sliceSize = min (fromIntegral rows - 1) (bufLength (s^.buf) - s^.rowoff)
        let slice = sliceRows (s^.rowoff) sliceSize (s^.buf)
        drawText $ (unpackToText . mapRows (T.take (fromIntegral cols - 1) . T.drop (s^.coloff))) slice
        drawText $ T.replicate (fromIntegral rows - sliceSize - 1) (T.pack "~\n")
        moveCursor crow ccol
    else
      return ()

editorDrawCursor :: EState ()
editorDrawCursor = do
    s <- get
    renderCol <- editorCursorColToRenderCol
    lift $ updateWindow (s^.window) $ do
      moveCursor (toInteger (s^.cursor.row - s^.rowoff)) (toInteger (renderCol - s^.coloff))

editorOpen :: String -> EState ()
editorOpen filename = do
  fileExists <- liftIO $ doesFileExist filename
  if fileExists then do
    contents <- liftIO $ readFile filename
    buf .= pack contents
  else
    return ()

editorSave :: EState ()
editorSave = do
  s <- get
  liftIO $ writeFile (s^.filename) (unpackToString (s^.buf))

editorRun :: String -> IO ()
editorRun filename = do
    runCurses $ do
      setRaw True
      setEcho False

      w <- defaultWindow
      econf <- return EFields {
        _filename = filename,
        _window = w,
        _buf = empty,
        _rowoff = 0,
        _coloff = 0,
        _cursor = ECursor { _row = 0, _col = 0 },
        _flags = EFlags { _exit = False, _dirty = True },
        _conf = EConf { _tabStop = 8 }
        }

      flip runStateT econf $ do
        editorOpen filename

        whileM_ (liftM (not . _exit . _flags) get) $ do
          editorScroll
          editorDrawRows
          editorDrawCursor
          lift render
          ev <- get >>= lift . flip getEvent Nothing . _window
          case ev of
            Just ev' -> editorProcessEvent ev'
            Nothing -> return ()

      return ()
